var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');

var auth = require('./app/auth');
var userModel = require('./app/user');

var app = express();
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(bodyParser.json());

mongoose.connect('mongodb://localhost:27017/rest-api-auth-demo');

app.get('/', function(req, res){
  res.json({
    message: 'Server is up and running.',
    version : '1.0'
  });
});

app.post('/user/new', function(req, res){
  if(req.body.name && req.body.email && req.body.password) {
    var newUser = new userModel({
      'name' : req.body.name,
      'email' : req.body.email,
      'password' : req.body.password
    });
    newUser.save(function(err){
      if(err) {
        res.status(500).json({
          'success' : false,
          'message': 'internal error occured'
        });
      } else {
        res.status(200).json({
          'success' : true,
          'message' : 'user created!'
        });
      }
    });
  } else {
    res.status(400).json({
      'success' : false,
      'message' : 'all fields are required'
    });
  }
  return;
});

app.post('/user/login', function(req, res){
  if (req.body.email) {
    userModel.findOne({ //select user by email from db
      'email': req.body.email
    }, function (err, usr) {
      if (usr) {
        if (req.body.password) {
          usr.comparePassword(req.body.password, function (err, isMatch) {
            if (err) {
              res.status(500).json({
                'success': false,
                'message': 'Something went wrong'
              });
            } else {
              if (isMatch) {
                //This payload is what we get latter we decoded this token.
                const payload = {
                  email: usr.email
                };
                var token = jwt.sign(payload, 'myappsecreatkey', {
                  expiresIn: 1440 // expires in 24 hours
                });
                res.status(200).json({
                  'success': true,
                  'message': 'Login credentials verified',
                  'token': token
                });
              } else {
                res.status(401).json({
                  'success': false,
                  'message': 'Incorrect password'
                });
              }
            }
          });
        } else {
          res.status(400).json({
            'success': false,
            'message': 'Please provide password'
          });
        }
      } else {
        res.status(404).json({
          'success': false,
          'message': 'Such a user does not exist'
        });
      }

    });
  } else {
    res.status(400).json({
      'success': false,
      'message': 'Please provide user details'
    });
  }
});

app.get('/user/private-data', auth.verifyToken, function(req,res){
  if (req.decoded.email) { //req.decoded will have data from decoded token by our auth middlerware.
    userModel.findOne({ //selecting user again. Just to make sure that he is alive.
        'email': req.decoded.email
      }, function (err, usr) {
        if (err) {
          res.status(500).json({
            'success': false,
            'message': err
          });
        } else {
          if (usr) {
            res.status(200).json({
              'success': true,
              'message': 'You are verified. Here is your private data.',
              'data': usr
            });
          } else {
            res.status(204).json({
              'success': false,
              'message': 'Such a user does not exist'
            });
          }
        }
      });
  } else {
    res.status(400).json({
      'success': false,
      'message': 'Please login first'
    });
  }
});

app.listen('3000', function(){
  console.log('server up and running at 3000 port');
});
