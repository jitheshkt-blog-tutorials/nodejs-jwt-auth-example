var jwt = require('jsonwebtoken');

module.exports = {
    verifyToken: function (req, res, next) {
        var token = req.body.token || req.query.token || req.headers.authorization; //get token from request.
        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, 'myappsecreatkey', function (err, decoded) {
                console.log(err);
                if (err) {
                    res.status(403).json({
                        success: false,
                        message: 'Failed to authenticate token.'
                    });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {
            // if there is no token return the 403.
            return res.status(403).send({
                success: false,
                message: 'No token provided.'
            });
        }
    }
};